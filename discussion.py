# class Car():
#     def __init__(self, brand, model, year_of_make):
#         self.brand = brand
#         self.model = model
#         self.year_of_make = year_of_make

#         self.fuel = "Gasoline"
#         self.fuel_level = 0

#     def __repr__(self):
#         return  '"brand": self.brand,\n"model": self.model,\n"year_of_make": self.year_of_make'

#     # methods
#     def fill_fuel(self):
#         print(f"Current fuel level: {self.fuel_level}")
#         print(f"Filling up the fuel tank...")
#         self.fuel_level = 100
#         print(f"New fuel level: {self.fuel_level}")

# new_car = Car("Honda","Jazz", 2020)
# print(f"My car is a {new_car.brand} {new_car.model} {new_car.year_of_make}")
# print(new_car)

# [SECTION] Encapsulation
# a form of data hiding

class Person():
    def __init__(self):
        # _ denots that these attributes must onle be modified and retrieved by getters and setters
        self._name = "John Doe"
        self._age = 0

    def set_name(self, name):
        self._name = name

    def get_name(self):
        print(f"Name of Person: {self._name}")

    def set_age(self, age):
        self._age = age
    
    def get_age(self):
        print(f"Age of Person: {self._age}")

p1 = Person()
p1.get_name()
p1.get_age()

p1.set_name("Jane Doe")
p1.set_age(18)

p1.get_name()
p1.get_age()

# [Section] Inheritance
class Employee(Person):
    def __init__(self, employee_id):
        # super() can be used to invoke immediate parent class constructor
        super().__init__()
        self._employee_id = employee_id

    def get_employee_id(self):
        print(f"The employee id is {self._employee_id}")

    def set_employee_id(self, employee_id):
        self._employee_id = employee_id

    def get_details(self):
        print(f"{self._employee_id} belongs to {self._name}")

emp1 = Employee("EMP-001")
emp1.get_details()

class Student(Person):
    def __init__(self, student_number, course, year_level):
        super().__init__()
        self._student_number = student_number
        self._course = course
        self._year_level = year_level

    def get_student_number(self):
        print(f"Student number: {self._student_number}")

    def set_student_number(self, student_number):
        self._student_number = student_number

    def get_course(self):
        print(f"Course: {self._course}")

    def set_course(self, course):
        self._course = course

    def get_year_level(self):
        print(f"Year level: {self._year_level}")

    def set_year_level(self, year_level):
        self._year_level = year_level

    def get_details(self):
        print(f"{self._name} is currently in {self._year_level} taking up {self._course}")

student_1 = Student("ST-001", "BSIT", "4th year")
student_1.get_details()

# [Section] Polymorphism
# re-defintion of methods after inheritance

# Using Function
class Admin():
    def is_admin(self):
        print(True)

    def user_type(self):
        print("Admin User")

class Customer():
    def is_admin(self):
        print(False)

    def user_type(self):
        print("Regular User")

# Define a function that will check the taken object as argument
def test_function(obj):
    obj.is_admin()
    obj.user_type()

user_admin = Admin()
user_customer = Customer()

# what happens is that the test_function would call methods of the objects passed to it hence allowing it to have different outputs depending on the object
test_function(user_admin)
test_function(user_customer)

# Using Classes
class Team_Lead():
    def occupation(self):
        print("Team Lead")

    def has_auth(self):
        print(True)

class Team_Member():
    def occupation(self):
        print("Team Member")

    def has_auth(self):
        print(False)

tl1 = Team_Lead()
tm1 = Team_Member()

# using for loop to iterate in a sequence of classes
for person in (tl1, tm1):
    # will access the occupation method for each of the iterated class
    person.occupation()


# Using Inheritance
class Zuitt():
    def tracks(self):
        print("We are currently offering 3 tracks (developer career, pi-shape, short courses")

    def num_of_hours(self):
        print("Learn web dev in 360 hours")

class Developer_Career(Zuitt):
    def num_of_hours(self):
        print("Learn the basics of web development in 240 hours")

class Pi_Shaped(Zuitt):
    def num_of_hours(self):
        print("Learn the basics of web development in 140 hours")

class Short_Course(Zuitt):
    def num_of_hours(self):
        print("Learn the basics of web development in 20 hours")

course1 = Developer_Career()
course2 = Pi_Shaped()
course3 = Short_Course()

for course in (course1, course2, course3):
    course.num_of_hours()
    course.tracks()


# [Section] Abstraction
from abc import ABC, abstractclassmethod

class Polygon(ABC):
    @abstractclassmethod
    def print_number_of_sides(self):
        pass # denotes that the method does not do anything

class Triangle(Polygon):
    def __init__(self):
        super().__init__()

    def print_number_of_sides(self):
        print(f"This polygon has 3 sides")


class Pentagon(Polygon):
    def __init__(self):
        super().__init__()

    def print_number_of_sides(self):
        print(f"This polygon has 5 sides")

shape1 = Triangle()
shape2 = Pentagon()
shape1.print_number_of_sides()
shape2.print_number_of_sides()
