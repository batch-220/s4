# Abstract Class
from abc import ABC, abstractclassmethod

class Animal_Methods(ABC):
    @abstractclassmethod
    def eat(self, food):
        pass

    @abstractclassmethod
    def make_sound(self):
        pass

    @abstractclassmethod
    def call(self):
        pass


class Animal(Animal_Methods):
    def __init__(self, name, breed, age):
        self._name = name
        self._breed = breed
        self._age = age

    # Getters
    def get_name(self):
        return self._name

    def get_breed(self):
        return self._breed

    def get_age(self):
        return self._age

    # Setters
    def set_name(self, name):
        self._name = name

    def set_breed(self, breed):
        self._breed = breed

    def set_age(self, age):
        self._age = age



class Dog(Animal):
    def eat(self, food):
        print(f"Eaten {food}")

    def make_sound(self):
        print("Bark! Woof! Arf!")

    def call(self):
        print(f"Here {self._name}")


class Cat(Animal):
    def eat(self, food):
        print(f"Serve me {food}")

    def make_sound(self):
        print("Miaow! Nyaw! Nyaaaaa")

    def call(self):
        print(f"{self._name} come on!")




# Test Cases
dog1 = Dog("Isis", "Dalmatian", 15)
dog1.eat("Steak")
dog1.make_sound()
dog1.call()

print('\n') # line break for output readability

cat1 = Cat("Puss", "Persian", 4)
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()

